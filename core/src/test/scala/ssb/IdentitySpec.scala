package ssb

import org.scalatest._
import org.scalatest.flatspec.AnyFlatSpec
import ssb.crypto.Ed25519

class IdentitySpec extends AnyFlatSpec {

  import Identity._

  "Identity" should "contain id" in {
    given ed25519: Ed25519 = Ed25519()
    val identity1 = Identity()
    val id1 = identity1.id
    assert(null != id1)
    assert(id1.nonEmpty)
    assertResult(44)(id1.length())

    val identity2 = Identity()
    val id2 = identity2.id
    assert(null != id2)
    assert(id2.nonEmpty)
    assertResult(44)(id2.length())

    assertResult(id1)(id2)
  }
}
