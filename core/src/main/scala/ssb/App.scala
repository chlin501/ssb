package ssb

import org.slf4j.LoggerFactory
import java.nio.file.Path

val log = LoggerFactory.getLogger(classOf[App])
val ssbDirPrefix = ".ssb"

val ssbDir: () => Path = () => Path.of(ssbDirPrefix).toAbsolutePath()

@main def run = {
  val ssbDirPath = ssbDir()
  log.info("Hey, this is ssb!")
}
