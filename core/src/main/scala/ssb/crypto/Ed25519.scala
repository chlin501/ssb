package ssb.crypto

import org.bouncycastle.crypto.AsymmetricCipherKeyPair
import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator
import org.bouncycastle.crypto.KeyGenerationParameters
import java.security.SecureRandom
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters
import java.util.Base64

object Ed25519 {

  def apply(
      bitsSizeOfKeys: Int = 255,
      s: => SecureRandom = new SecureRandom
  ): Ed25519 = {
    val gen = new Ed25519KeyPairGenerator
    gen.init(new KeyGenerationParameters(s, bitsSizeOfKeys))
    Ed25519(gen.generateKeyPair)
  }

}
final case class Ed25519(keypair: AsymmetricCipherKeyPair) {
  def id(): String = {
    new String(
      Base64
        .getUrlEncoder()
        .encode(
          keypair
            .getPublic()
            .asInstanceOf[Ed25519PublicKeyParameters]
            .getEncoded()
        )
    )
  }
}
