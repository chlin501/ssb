package ssb

import org.bouncycastle.crypto.AsymmetricCipherKeyPair
import ssb.crypto.Ed25519
import java.net.URL

object Identity {

  opaque type Name = String

  def apply()(using ed25519: Ed25519): Identity = Identity(ed25519.id())

}
final case class Identity(
    id: String,
    name: Option[Identity.Name] = None,
    avatar: Option[URL] = None
)(using Ed25519) {

  def withName(name: Identity.Name): Identity = copy(name = Option(name))

  def withAvatar(avatar: URL): Identity = copy(avatar = Option(avatar))

  override def toString(): String = s"@${id}.ed25519"
}
