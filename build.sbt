scalaVersion := "3.4.0"

lazy val core = project
  .in(file("core"))
  .settings(
    scalaVersion := "3.4.0",
    libraryDependencies ++= Seq(
      "org.bouncycastle" % "bcpkix-jdk18on" % "1.78.1",
      "org.slf4j" % "slf4j-api" % "2.0.16",
      "org.slf4j" % "slf4j-simple" % "2.0.16",
      "org.scalatest" %% "scalatest" % "3.2.19" % "test"
    )
  )

lazy val global = project
  .in(file("."))
  .aggregate(core)
